import Router from 'next/router';
import Cookies from 'universal-cookie';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Fragment } from 'react';
import * as operations from '../../../store/operations';
import Title from '../../UI/Title/Title';

const Home = ({products, token, isLoading, sortByName, searchByName, loadData}) => {
  const section = 'Holiers !';
  return (
    <Fragment>
      <Title text={section} isBold={true} centered={true} />
    </Fragment>
  )
};

Home.propTypes = {
  products: PropTypes.array.isRequired,
  loadData: PropTypes.func,
  isLoading: PropTypes.bool,
  sortByName: PropTypes.func.isRequired,
  searchByName: PropTypes.func.isRequired,
};

Home.defaultProps = {
  isLoading: false,
  loadData: () => {}
};

Home.getInitialProps = async ({store, req}) => {
  if (req) {
    const cookies = new Cookies(req.headers.cookie);
    const cookie = cookies.get('vintraAT') ? cookies.get('vintraAT') : null;
    await store.dispatch(operations.getData(cookie));
  }
  return {};
};

export default Home;