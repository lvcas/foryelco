module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./apis/services.js":
/*!**************************!*\
  !*** ./apis/services.js ***!
  \**************************/
/*! exports provided: charactersFetch, searchCharactersByName, getCharacter, filmsFetch, searchFilmsByName, getFilm, planetsFetch, searchPlanetsByName, getPlanet, vehiclesFetch, searchVehiclesByName, getVehicle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "charactersFetch", function() { return charactersFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchCharactersByName", function() { return searchCharactersByName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharacter", function() { return getCharacter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filmsFetch", function() { return filmsFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchFilmsByName", function() { return searchFilmsByName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilm", function() { return getFilm; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "planetsFetch", function() { return planetsFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchPlanetsByName", function() { return searchPlanetsByName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanet", function() { return getPlanet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vehiclesFetch", function() { return vehiclesFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchVehiclesByName", function() { return searchVehiclesByName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehicle", function() { return getVehicle; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

const BASE_API = 'https://swapi.co/api/';
const PEOPLE_API = `${BASE_API}people/`;
const PEOPLE_SEARCH_API = `${PEOPLE_API}?search=`;
const FILMS_API = `${BASE_API}films/`;
const FILMS_SEARCH_API = `${FILMS_API}?search=`;
const PLANETS_API = `${BASE_API}planets/`;
const PLANETS_SEARCH_API = `${PLANETS_API}?search=`;
const VEHICLES_API = `${BASE_API}vehicles/`;
const VEHICLES_SEARCH_API = `${VEHICLES_API}?search=`;
const charactersFetch = url => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url ? url : PEOPLE_API).then(response => response.data);
const searchCharactersByName = name => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(PEOPLE_SEARCH_API + name).then(response => response.data);
const getCharacter = id => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(PEOPLE_API + id).then(response => response);
const filmsFetch = url => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url ? url : FILMS_API).then(response => response.data);
const searchFilmsByName = name => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(FILMS_SEARCH_API + name).then(response => response.data);
const getFilm = id => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(FILMS_API + id).then(response => response);
const planetsFetch = url => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url ? url : PLANETS_API).then(response => response.data);
const searchPlanetsByName = name => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(PLANETS_SEARCH_API + name).then(response => response.data);
const getPlanet = id => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(PLANETS_API + id).then(response => response);
const vehiclesFetch = url => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url ? url : VEHICLES_API).then(response => response.data);
const searchVehiclesByName = name => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(VEHICLES_SEARCH_API + name).then(response => response.data);
const getVehicle = id => axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(VEHICLES_API + id).then(response => response);

/***/ }),

/***/ "./components/Button/Button.js":
/*!*************************************!*\
  !*** ./components/Button/Button.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Button_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Button.styles */ "./components/Button/Button.styles.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Button = ({
  text,
  isDisabled,
  onClick
}) => {
  const onClickButton = () => {
    if (!isDisabled) {
      onClick();
    }
  };

  return __jsx(_Button_styles__WEBPACK_IMPORTED_MODULE_2__["Btn"], {
    onClick: onClickButton,
    isDisabled: isDisabled
  }, text);
};

Button.propTypes = {
  text: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  isDisabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
Button.defaultProps = {
  isDisabled: false,
  onClick: () => {}
};
/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ }),

/***/ "./components/Button/Button.styles.js":
/*!********************************************!*\
  !*** ./components/Button/Button.styles.js ***!
  \********************************************/
/*! exports provided: Btn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Btn", function() { return Btn; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/colors */ "./static/styles/colors.js");
/* harmony import */ var _static_styles_devices__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../static/styles/devices */ "./static/styles/devices.js");



const Btn = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.button.withConfig({
  displayName: "Buttonstyles__Btn",
  componentId: "wrfgcc-0"
})(["padding:10px;color:", ";cursor:", ";border-color:", ";background-color:", ";font-size:14px;@media ", "{width:49%;}@media ", "{width:auto;margin-left:14px;}"], props => props.isDisabled ? _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["OLD_GREY"] : _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["WHITE"], props => props.isDisabled ? 'default' : 'pointer', props => props.isDisabled ? _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["OLD_GREY"] : _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["WHITE"], _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["BLACK"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_2__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_2__["device"].tablet);

/***/ }),

/***/ "./components/InputSearch/InputSearch.js":
/*!***********************************************!*\
  !*** ./components/InputSearch/InputSearch.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _InputSearch_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./InputSearch.styles */ "./components/InputSearch/InputSearch.styles.js");
/* harmony import */ var _Button_Button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Button/Button */ "./components/Button/Button.js");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const inputSearch = ({
  placeholder,
  onSearch,
  onReset
}) => {
  const {
    0: isDisabled,
    1: setDisabledStatus
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const {
    0: inputValue,
    1: setInputValue
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])('');

  const onChange = e => {
    const {
      value
    } = e.target;
    setInputValue(value);
    setDisabledStatus(value.length > 1 ? false : true);
  };

  const onClickSearch = () => {
    onSearch(inputValue);
  };

  const onClickReset = () => {
    setInputValue('');
    setDisabledStatus(true);
    onReset();
  };

  return __jsx(_InputSearch_styles__WEBPACK_IMPORTED_MODULE_2__["SearchContainer"], null, __jsx(_InputSearch_styles__WEBPACK_IMPORTED_MODULE_2__["Input"], {
    type: "text",
    placeholder: placeholder,
    value: inputValue,
    onChange: onChange
  }), __jsx(_InputSearch_styles__WEBPACK_IMPORTED_MODULE_2__["ButtonContainer"], null, __jsx(_Button_Button__WEBPACK_IMPORTED_MODULE_3__["default"], {
    text: "Search",
    onClick: onClickSearch,
    isDisabled: isDisabled
  }), __jsx(_Button_Button__WEBPACK_IMPORTED_MODULE_3__["default"], {
    text: "Reset",
    onClick: onClickReset,
    isDisabled: isDisabled
  })));
};

inputSearch.propTypes = {
  placeholder: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  onSearch: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onReset: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
inputSearch.defaultProps = {
  placeholder: '',
  onSearch: () => {},
  onReset: () => {}
};
/* harmony default export */ __webpack_exports__["default"] = (inputSearch);

/***/ }),

/***/ "./components/InputSearch/InputSearch.styles.js":
/*!******************************************************!*\
  !*** ./components/InputSearch/InputSearch.styles.js ***!
  \******************************************************/
/*! exports provided: SearchContainer, Input, ButtonContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchContainer", function() { return SearchContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonContainer", function() { return ButtonContainer; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/devices */ "./static/styles/devices.js");


const SearchContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "InputSearchstyles__SearchContainer",
  componentId: "frk2wq-0"
})(["display:flex;align-items:center;justify-content:space-around;margin:0 auto;margin-bottom:22px;@media ", "{flex-direction:column;}@media ", "{flex-direction:row;max-width:500px;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);
const Input = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.input.withConfig({
  displayName: "InputSearchstyles__Input",
  componentId: "frk2wq-1"
})(["height:30px;padding-left:12px;font-family:'Roboto',sans-serif;@media ", "{width:100%;}@media ", "{width:350px;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);
const ButtonContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "InputSearchstyles__ButtonContainer",
  componentId: "frk2wq-2"
})(["display:flex;justify-content:space-between;@media ", "{width:100%;}@media ", "{width:auto;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);

/***/ }),

/***/ "./components/Item/Item.js":
/*!*********************************!*\
  !*** ./components/Item/Item.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Item_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Item.styles */ "./components/Item/Item.styles.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Item = ({
  children
}) => __jsx(_Item_styles__WEBPACK_IMPORTED_MODULE_1__["ItemContainer"], null, children);

Item.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node
};
/* harmony default export */ __webpack_exports__["default"] = (Item);

/***/ }),

/***/ "./components/Item/Item.styles.js":
/*!****************************************!*\
  !*** ./components/Item/Item.styles.js ***!
  \****************************************/
/*! exports provided: ItemContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemContainer", function() { return ItemContainer; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/colors */ "./static/styles/colors.js");


const ItemContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Itemstyles__ItemContainer",
  componentId: "a74njx-0"
})(["padding:14px;display:flex;flex-direction:row;cursor:pointer;border-bottom:1px solid ", ";font-family:'Roboto',sans-serif;font-size:22px;transition:0.25s;color:", ";&:hover{color:", ";}"], _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["OLD_GREY"], _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["OLD_GREY"], _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["WHITE"]);

/***/ }),

/***/ "./components/List/List.js":
/*!*********************************!*\
  !*** ./components/List/List.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _NavLink_NavLink__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../NavLink/NavLink */ "./components/NavLink/NavLink.js");
/* harmony import */ var _List_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./List.styles */ "./components/List/List.styles.js");
/* harmony import */ var _Title_Title__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Title/Title */ "./components/Title/Title.js");
/* harmony import */ var _Item_Item__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Item/Item */ "./components/Item/Item.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const List = ({
  list,
  filterBy,
  section
}) => {
  const sectionName = section.toLowerCase();
  const filterName = filterBy.toLowerCase();
  return __jsx(_List_styles__WEBPACK_IMPORTED_MODULE_3__["Grid"], null, list ? list.map(item => {
    const index = item.url.indexOf(filterName) + filterName.length;
    const id = item.url.substring(index + 1, item.url.length - 1);
    return __jsx(_NavLink_NavLink__WEBPACK_IMPORTED_MODULE_2__["default"], {
      key: item.url,
      href: `${sectionName}/${id}/`,
      as: `${sectionName}/${id}/`
    }, __jsx(_Item_Item__WEBPACK_IMPORTED_MODULE_5__["default"], null, item.name ? item.name : item.title));
  }) : __jsx(_Title_Title__WEBPACK_IMPORTED_MODULE_4__["default"], {
    text: "No results Found :("
  }));
};

List.propTypes = {
  list: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
    name: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string
  })),
  filterBy: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  section: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (List);

/***/ }),

/***/ "./components/List/List.styles.js":
/*!****************************************!*\
  !*** ./components/List/List.styles.js ***!
  \****************************************/
/*! exports provided: Grid */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Grid", function() { return Grid; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/colors */ "./static/styles/colors.js");


const Grid = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Liststyles__Grid",
  componentId: "qaglco-0"
})(["background-color:", ";div{border-top:none;}"], _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["BLACK"]);

/***/ }),

/***/ "./components/NavLink/NavLink.js":
/*!***************************************!*\
  !*** ./components/NavLink/NavLink.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _NavLink_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./NavLink.styles */ "./components/NavLink/NavLink.styles.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const NavLink = ({
  children,
  href,
  as
}) => __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
  href: href,
  as: as
}, __jsx(_NavLink_styles__WEBPACK_IMPORTED_MODULE_3__["A"], null, children));

NavLink.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node.isRequired,
  href: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string.isRequired,
  as: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (NavLink);

/***/ }),

/***/ "./components/NavLink/NavLink.styles.js":
/*!**********************************************!*\
  !*** ./components/NavLink/NavLink.styles.js ***!
  \**********************************************/
/*! exports provided: A */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "A", function() { return A; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);

const A = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.a.withConfig({
  displayName: "NavLinkstyles__A",
  componentId: "sc-11jmyh2-0"
})(["cursor:pointer;text-decoration:none;font-family:'Roboto',sans-serif;"]);

/***/ }),

/***/ "./components/Pagination/Pagination.js":
/*!*********************************************!*\
  !*** ./components/Pagination/Pagination.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Pagination_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Pagination.styles */ "./components/Pagination/Pagination.styles.js");
/* harmony import */ var _Button_Button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Button/Button */ "./components/Button/Button.js");
/* harmony import */ var _Title_Title__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Title/Title */ "./components/Title/Title.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const Pagination = ({
  totalPages,
  hasPrevPage,
  hasNextPage,
  onClickPrev,
  onClickNext
}) => __jsx(_Pagination_styles__WEBPACK_IMPORTED_MODULE_2__["PaginationContainer"], null, __jsx(_Pagination_styles__WEBPACK_IMPORTED_MODULE_2__["TitleContainer"], null, __jsx(_Title_Title__WEBPACK_IMPORTED_MODULE_4__["default"], {
  text: totalPages
})), __jsx(_Pagination_styles__WEBPACK_IMPORTED_MODULE_2__["ButtonContainer"], null, __jsx(_Button_Button__WEBPACK_IMPORTED_MODULE_3__["default"], {
  isDisabled: !hasPrevPage,
  onClick: onClickPrev,
  text: "Previous"
}), __jsx(_Button_Button__WEBPACK_IMPORTED_MODULE_3__["default"], {
  isDisabled: !hasNextPage,
  onClick: onClickNext,
  text: "Next"
})));

Pagination.propTypes = {
  totalPages: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  hasPrevPage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string]),
  hasNextPage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string]),
  onClickPrev: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onClickNext: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
Pagination.defaultProps = {
  hasPrevPage: false,
  hasNextPage: false,
  onClickPrev: () => {},
  onClickNext: () => {}
};
/* harmony default export */ __webpack_exports__["default"] = (Pagination);

/***/ }),

/***/ "./components/Pagination/Pagination.styles.js":
/*!****************************************************!*\
  !*** ./components/Pagination/Pagination.styles.js ***!
  \****************************************************/
/*! exports provided: PaginationContainer, TitleContainer, ButtonContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationContainer", function() { return PaginationContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleContainer", function() { return TitleContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonContainer", function() { return ButtonContainer; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/devices */ "./static/styles/devices.js");


const PaginationContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Paginationstyles__PaginationContainer",
  componentId: "wkduo9-0"
})(["padding-left:14px;padding-right:14px;@media ", "{display:flex;flex-direction:column;align-items:center;justify-content:space-between;}@media ", "{flex-direction:row;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);
const TitleContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Paginationstyles__TitleContainer",
  componentId: "wkduo9-1"
})(["@media ", "{width:100%;}@media ", "{width:auto;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);
const ButtonContainer = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Paginationstyles__ButtonContainer",
  componentId: "wkduo9-2"
})(["display:flex;justify-content:space-between;@media ", "{width:100%;}@media ", "{width:auto;}"], _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].mobile, _static_styles_devices__WEBPACK_IMPORTED_MODULE_1__["device"].tablet);

/***/ }),

/***/ "./components/Title/Title.js":
/*!***********************************!*\
  !*** ./components/Title/Title.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Title_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Title.styles */ "./components/Title/Title.styles.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Title = ({
  text,
  centered,
  isBold,
  isSmall
}) => __jsx(_Title_styles__WEBPACK_IMPORTED_MODULE_2__["Text"], {
  isBold: isBold,
  centered: centered,
  isSmall: isSmall
}, text);

Title.propTypes = {
  text: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  centered: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  isBold: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  isSmall: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
Title.defaultProps = {
  isSmall: false,
  isBold: false,
  centered: false
};
/* harmony default export */ __webpack_exports__["default"] = (Title);

/***/ }),

/***/ "./components/Title/Title.styles.js":
/*!******************************************!*\
  !*** ./components/Title/Title.styles.js ***!
  \******************************************/
/*! exports provided: Text */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Text", function() { return Text; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../static/styles/colors */ "./static/styles/colors.js");


const Text = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.p.withConfig({
  displayName: "Titlestyles__Text",
  componentId: "sc-1olre68-0"
})(["margin-bottom:", ";padding-left:14px;padding-right:14px;font-family:'Roboto',sans-serif;color:", ";font-size:", ";text-align:", ";font-weight:", ";"], props => props.isSmall ? '0' : '22px', _static_styles_colors__WEBPACK_IMPORTED_MODULE_1__["PRIMARY"], props => props.isSmall ? '16px' : '22px', props => props.centered ? 'center' : 'left', props => props.isBold ? 'bold' : 'normal');

/***/ }),

/***/ "./containers/Characters/index.js":
/*!****************************************!*\
  !*** ./containers/Characters/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store_operations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../store/operations */ "./store/operations.js");
/* harmony import */ var _components_List_List__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/List/List */ "./components/List/List.js");
/* harmony import */ var _components_Title_Title__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Title/Title */ "./components/Title/Title.js");
/* harmony import */ var _components_Pagination_Pagination__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/Pagination/Pagination */ "./components/Pagination/Pagination.js");
/* harmony import */ var _components_InputSearch_InputSearch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/InputSearch/InputSearch */ "./components/InputSearch/InputSearch.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








const Characters = ({
  characters,
  total,
  isLoading,
  nextPage,
  prevPage,
  searchByName,
  loadMore
}) => {
  const totalCharacters = `Total characters: ${total}`;
  const section = "PEOPLE";
  const sectionName = "character";
  const loading = "Loading...";

  const onSearch = name => {
    searchByName(name);
  };

  const onReset = () => {
    loadMore();
  };

  const onClickPrev = () => {
    if (prevPage) {
      loadMore(prevPage);
    }
  };

  const onClickNext = () => {
    if (nextPage) {
      loadMore(nextPage);
    }
  };

  return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx(_components_Title_Title__WEBPACK_IMPORTED_MODULE_4__["default"], {
    text: section,
    isBold: true,
    centered: true
  }), __jsx(_components_InputSearch_InputSearch__WEBPACK_IMPORTED_MODULE_6__["default"], {
    placeholder: "Search a character !",
    onSearch: onSearch,
    onReset: onReset
  }), isLoading ? __jsx(_components_Title_Title__WEBPACK_IMPORTED_MODULE_4__["default"], {
    text: loading,
    isBold: true,
    centered: true
  }) : __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx(_components_List_List__WEBPACK_IMPORTED_MODULE_3__["default"], {
    list: characters,
    section: sectionName,
    filterBy: section
  }), __jsx(_components_Pagination_Pagination__WEBPACK_IMPORTED_MODULE_5__["default"], {
    totalPages: totalCharacters,
    hasPrevPage: prevPage,
    hasNextPage: nextPage,
    onClickPrev: onClickPrev,
    onClickNext: onClickNext
  })));
};

Characters.propTypes = {
  characters: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array.isRequired,
  isLoading: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired,
  total: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number,
  nextPage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  prevPage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  loadMore: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  searchByName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};
Characters.defaultProps = {
  total: 1,
  nextPage: '',
  prevPage: ''
};

Characters.getInitialProps = async ({
  store
}) => {
  await store.dispatch(_store_operations__WEBPACK_IMPORTED_MODULE_2__["fetchCharacters"]());
  return {};
};

/* harmony default export */ __webpack_exports__["default"] = (Characters);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/map.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/map.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/map */ "core-js/library/fn/map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/create.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/create */ "core-js/library/fn/object/create");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-properties */ "core-js/library/fn/object/define-properties");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptors */ "core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-symbols */ "core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/keys */ "core-js/library/fn/object/keys");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/promise.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/promise.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/promise */ "core-js/library/fn/promise");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/weak-map.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/weak-map */ "core-js/library/fn/weak-map");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/extends.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/extends.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__(/*! ../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _Object$getOwnPropertyDescriptor = __webpack_require__(/*! ../core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");

var _Object$defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var _WeakMap = __webpack_require__(/*! ../core-js/weak-map */ "./node_modules/@babel/runtime-corejs2/core-js/weak-map.js");

function _getRequireWildcardCache() {
  if (typeof _WeakMap !== "function") return null;
  var cache = new _WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};

  if (obj != null) {
    var hasPropertyDescriptor = _Object$defineProperty && _Object$getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          _Object$defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = void 0;

var _map = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/map */ "./node_modules/@babel/runtime-corejs2/core-js/map.js"));

var _url = __webpack_require__(/*! url */ "url");

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _propTypes = _interopRequireDefault(__webpack_require__(/*! prop-types */ "prop-types"));

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _rewriteUrlForExport = __webpack_require__(/*! ../next-server/lib/router/rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "./node_modules/next/dist/next-server/lib/utils.js");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new _map.default();
var IntersectionObserver = false ? undefined : null;

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: formatUrl(href),
        as: asHref ? formatUrl(asHref) : asHref
      };
    });

    this.linkClicked = e => {
      // @ts-ignore target exists on currentTarget
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      this.cleanUpListeners = listenToIntersections(ref, () => {
        this.prefetch();
      });
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch() {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var {
      pathname
    } = window.location;
    var {
      href: parsedHref
    } = this.formatUrls(this.props.href, this.props.as);
    var href = (0, _url.resolve)(pathname, parsedHref);

    _router.default.prefetch(href);
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch();
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) {}

    return _react.default.cloneElement(child, props);
  }

}

Link.propTypes = void 0;

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact");

  Link.propTypes = exact({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _defineProperty = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "./node_modules/next/dist/next-server/lib/router-context.js");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

(0, _defineProperty.default)(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  (0, _defineProperty.default)(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = (0, _extends2.default)({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _extends2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/extends */ "./node_modules/@babel/runtime-corejs2/helpers/extends.js"));

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, (0, _extends2.default)({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouterWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouterWrapper;
}

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/mitt.js":
/*!********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/mitt.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

var _Object$create = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/create */ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = _Object$create(null);

  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router-context.js":
/*!******************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router-context.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__(/*! react */ "react"));

exports.RouterContext = React.createContext(null);

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function rewriteUrlForNextExport(url) {
  const [pathname, hash] = url.split('#'); // tslint:disable-next-line

  let [path, qs] = pathname.split('?');
  path = path.replace(/\/$/, ''); // Append a trailing slash if this path does not have an extension

  if (!/\.[^/]+\/?$/.test(path)) path += `/`;
  if (qs) path += '?' + qs;
  if (hash) path += '#' + hash;
  return path;
}

exports.rewriteUrlForNextExport = rewriteUrlForNextExport;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/router.js":
/*!*****************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/router.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Promise = __webpack_require__(/*! @babel/runtime-corejs2/core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");

var _Object$assign = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "./node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "./node_modules/next/dist/next-server/lib/utils.js");

const rewrite_url_for_export_1 = __webpack_require__(/*! ./rewrite-url-for-export */ "./node_modules/next/dist/next-server/lib/router/rewrite-url-for-export.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription
  }) {
    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.url === this.pathname && e.state.as === this.asPath) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented
    // @ts-ignore backwards compatibility

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    return rewrite_url_for_export_1.rewriteUrlForNextExport(url);
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = _Object$assign({}, data, {
      Component
    });

    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new _Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.SUPPORTS_PERFORMANCE_USER_TIMING) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      const url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as; // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      } // @ts-ignore pathname is always a string


      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const rr = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(rr)(asPathname);

        if (!routeMatch) {
          const error = 'The provided `as` value is incompatible with the `href` value. This is invalid. https://err.sh/zeit/next.js/incompatible-href-as';

          if (true) {
            throw new Error(error);
          } else {}

          return resolve(false);
        } // Merge params into `query`, overwriting any specified in search


        _Object$assign(query, routeMatch);
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result
      // @ts-ignore pathname is always a string

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);
        const hash = window.location.hash.substring(1);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        } // @ts-ignore pathname is always defined


        this.set(route, pathname, query, as, _Object$assign({}, routeInfo, {
          hash
        }));

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      } // @ts-ignore method should always exist on history


      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      // @ts-ignore method should always exist on history
      window.history[method]({
        url,
        as,
        options
      }, null, as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return _Promise.resolve(cachedRouteInfo);
    }

    return new _Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(Component => resolve({
        Component
      }), reject);
    }).then(routeInfo => {
      const {
        Component
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "react-is");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return new _Promise((resolve, reject) => {
        // we provide AppTree later so this needs to be `any`
        this.getInitialProps(Component, {
          pathname,
          query,
          asPath: as
        }).then(props => {
          routeInfo.props = props;
          this.components[route] = routeInfo;
          resolve(routeInfo);
        }, reject);
      });
    }).catch(err => {
      return new _Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR') {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(Component => {
          const routeInfo = {
            Component,
            err
          };
          return new _Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }));
      });
    });
  }

  set(route, pathname, query, as, data) {
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch `page` code, you may wait for the data during `page` rendering.
   * This feature only works in production!
   * @param url of prefetched `page`
   */


  prefetch(url) {
    return new _Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) return; // @ts-ignore pathname is always defined

      const route = toRoute(pathname);
      this.pageLoader.prefetch(route).then(resolve, reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    const Component = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return Component;
  }

  async getInitialProps(Component, ctx) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    const {
      Component: App
    } = this.components['/_app'];
    let props;

    if (Component.__NEXT_SPR) {
      let status; // pathname should have leading slash

      let {
        pathname
      } = url_1.parse(ctx.asPath || ctx.pathname);
      pathname = !pathname || pathname === '/' ? '/index' : pathname;
      props = await fetch( // @ts-ignore __NEXT_DATA__
      `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`).then(res => {
        if (!res.ok) {
          status = res.status;
          throw new Error('failed to load prerender data');
        }

        return res.json();
      }).catch(err => {
        console.error(`Failed to load data`, status, err);
        window.location.href = pathname;
        return new _Promise(() => {});
      });
    } else {
      const AppTree = this._wrapApp(App);

      ctx.AppTree = AppTree;
      props = await utils_1.loadGetInitialProps(App, {
        AppTree,
        Component,
        router: this,
        ctx
      });
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    if (cancelled) {
      const err = new Error('Loading initial props cancelled');
      err.cancelled = true;
      throw err;
    }

    return props;
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

Router.events = mitt_1.default();
exports.default = Router;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!***************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string


const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!******************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const params = {};

    _Object$keys(groups).forEach(slugName => {
      const m = routeMatch[groups[slugName]];

      if (m !== undefined) {
        params[slugName] = m.indexOf('/') !== -1 ? m.split('/').map(entry => decodeURIComponent(entry)) : decodeURIComponent(m);
      }
    });

    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!****************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => (groups[$1 // Un-escape key
  .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
  ] = groupIndex++, $1.indexOf('\\.\\.\\.') === 0 ? '/(.+?)' : '/([^/]+?)'));
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");

var _Object$defineProperty = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result = null;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn.apply(this, args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  if (true) {
    if (App.prototype && App.prototype.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (_Object$keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      _Object$keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SUPPORTS_PERFORMANCE = typeof performance !== 'undefined';
exports.SUPPORTS_PERFORMANCE_USER_TIMING = exports.SUPPORTS_PERFORMANCE && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store_operations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/operations */ "./store/operations.js");
/* harmony import */ var _store_selectors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store/selectors */ "./store/selectors.js");
/* harmony import */ var _containers_Characters__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../containers/Characters */ "./containers/Characters/index.js");





const mapStateToProps = state => {
  return {
    characters: _store_selectors__WEBPACK_IMPORTED_MODULE_2__["getAllCharacters"](state),
    isLoading: _store_selectors__WEBPACK_IMPORTED_MODULE_2__["isLoadingCharacters"](state),
    total: _store_selectors__WEBPACK_IMPORTED_MODULE_2__["getTotalCharacters"](state),
    nextPage: _store_selectors__WEBPACK_IMPORTED_MODULE_2__["getCharactersNextPage"](state),
    prevPage: _store_selectors__WEBPACK_IMPORTED_MODULE_2__["getCharactersPreviousPage"](state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    searchByName: name => dispatch(_store_operations__WEBPACK_IMPORTED_MODULE_1__["searchCharacters"](name)),
    loadMore: url => dispatch(_store_operations__WEBPACK_IMPORTED_MODULE_1__["fetchCharacters"](url))
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_0__["connect"])(mapStateToProps, mapDispatchToProps)(_containers_Characters__WEBPACK_IMPORTED_MODULE_3__["default"]));

/***/ }),

/***/ "./static/styles/colors.js":
/*!*********************************!*\
  !*** ./static/styles/colors.js ***!
  \*********************************/
/*! exports provided: PRIMARY, SECONDARY, BLACK, WHITE, OLD_GREY */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRIMARY", function() { return PRIMARY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SECONDARY", function() { return SECONDARY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BLACK", function() { return BLACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHITE", function() { return WHITE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OLD_GREY", function() { return OLD_GREY; });
const PRIMARY = '#DC3545';
const SECONDARY = 'rgb(1, 108, 249)';
const BLACK = '#000000';
const WHITE = '#FFFFFF';
const OLD_GREY = '#999999';

/***/ }),

/***/ "./static/styles/devices.js":
/*!**********************************!*\
  !*** ./static/styles/devices.js ***!
  \**********************************/
/*! exports provided: device */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "device", function() { return device; });
const size = {
  mobile: '320px',
  tablet: '768px',
  desktop: '2560px'
};
const device = {
  mobile: `(min-width: ${size.mobile})`,
  tablet: `(min-width: ${size.tablet})`,
  desktop: `(min-width: ${size.desktop})`
};

/***/ }),

/***/ "./store/apiCall.js":
/*!**************************!*\
  !*** ./store/apiCall.js ***!
  \**************************/
/*! exports provided: API_CALL, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_CALL", function() { return API_CALL; });
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-properties */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptors */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-symbols */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");








function ownKeys(object, enumerableOnly) { var keys = _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default()(object); if (_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default.a) { var symbols = _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(target, key, source[key]); }); } else if (_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default.a) { _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default()(target, _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default()(source)); } else { ownKeys(source).forEach(function (key) { _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(target, key, _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(source, key)); }); } } return target; }

const API_CALL = 'API_CALL';

const apiCall = (baseActionType, apiCall, onSuccess, otherActionProps) => {
  return _objectSpread({
    type: API_CALL,
    baseActionType,
    apiCall,
    onSuccess
  }, otherActionProps);
};

/* harmony default export */ __webpack_exports__["default"] = (apiCall);

/***/ }),

/***/ "./store/characters/operations.js":
/*!****************************************!*\
  !*** ./store/characters/operations.js ***!
  \****************************************/
/*! exports provided: fetchCharacters, searchCharacters, getCharacterById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCharacters", function() { return fetchCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchCharacters", function() { return searchCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharacterById", function() { return getCharacterById; });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./types */ "./store/characters/types.js");
/* harmony import */ var _apis_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../apis/services */ "./apis/services.js");
/* harmony import */ var _apiCall__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../apiCall */ "./store/apiCall.js");



const fetchCharacters = url => {
  const URI = url ? Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["charactersFetch"])(url) : Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["charactersFetch"])();
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_CHARACTERS"], () => URI, data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const searchCharacters = name => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_CHARACTERS"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["searchCharactersByName"])(name), data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const getCharacterById = id => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["GET_CHARACTER_INFO"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["getCharacter"])(id), response => {
      const name = response.data.name;
      const height = response.data.height;
      const mass = response.data.mass;
      const hairColor = response.data.hair_color;
      const skinColor = response.data.skin_color;
      const eyeColor = response.data.eye_color;
      const birthYear = response.data.birth_year;
      const gender = response.data.gender;
      const homeworld = response.data.homeworld;
      const films = response.data.films;
      const vehicles = response.data.vehicles;
      return {
        name,
        height,
        mass,
        hairColor,
        skinColor,
        eyeColor,
        birthYear,
        gender,
        homeworld,
        films,
        vehicles
      };
    }));
  };
};

/***/ }),

/***/ "./store/characters/selectors.js":
/*!***************************************!*\
  !*** ./store/characters/selectors.js ***!
  \***************************************/
/*! exports provided: getAllCharacters, getCharacterById, isLoadingCharacters, getTotalCharacters, getCharactersNextPage, getCharactersPreviousPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllCharacters", function() { return getAllCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharacterById", function() { return getCharacterById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingCharacters", function() { return isLoadingCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalCharacters", function() { return getTotalCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharactersNextPage", function() { return getCharactersNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharactersPreviousPage", function() { return getCharactersPreviousPage; });
const getAllCharacters = characters => characters.items;
const getCharacterById = characters => characters.itemSelected;
const isLoadingCharacters = characters => characters.isLoading;
const getTotalCharacters = characters => characters.totalItems;
const getCharactersNextPage = characters => characters.next;
const getCharactersPreviousPage = characters => characters.previous;

/***/ }),

/***/ "./store/characters/types.js":
/*!***********************************!*\
  !*** ./store/characters/types.js ***!
  \***********************************/
/*! exports provided: FETCH_CHARACTERS, FETCH_CHARACTERS_START, FETCH_CHARACTERS_SUCCESS, FETCH_CHARACTERS_FAIL, GET_CHARACTER_INFO, GET_CHARACTER_INFO_START, GET_CHARACTER_INFO_SUCCESS, GET_CHARACTER_INFO_FAIL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_CHARACTERS", function() { return FETCH_CHARACTERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_CHARACTERS_START", function() { return FETCH_CHARACTERS_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_CHARACTERS_SUCCESS", function() { return FETCH_CHARACTERS_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_CHARACTERS_FAIL", function() { return FETCH_CHARACTERS_FAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_CHARACTER_INFO", function() { return GET_CHARACTER_INFO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_CHARACTER_INFO_START", function() { return GET_CHARACTER_INFO_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_CHARACTER_INFO_SUCCESS", function() { return GET_CHARACTER_INFO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_CHARACTER_INFO_FAIL", function() { return GET_CHARACTER_INFO_FAIL; });
const FETCH_CHARACTERS = 'FETCH_CHARACTERS';
const FETCH_CHARACTERS_START = 'FETCH_CHARACTERS_START';
const FETCH_CHARACTERS_SUCCESS = 'FETCH_CHARACTERS_SUCCESS';
const FETCH_CHARACTERS_FAIL = 'FETCH_CHARACTERS_FAIL';
const GET_CHARACTER_INFO = 'GET_CHARACTER_INFO';
const GET_CHARACTER_INFO_START = 'GET_CHARACTER_INFO_START';
const GET_CHARACTER_INFO_SUCCESS = 'GET_CHARACTER_INFO_SUCCESS';
const GET_CHARACTER_INFO_FAIL = 'GET_CHARACTER_INFO_FAIL';

/***/ }),

/***/ "./store/films/operations.js":
/*!***********************************!*\
  !*** ./store/films/operations.js ***!
  \***********************************/
/*! exports provided: fetchFilms, searchFilms, getFilmById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchFilms", function() { return fetchFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchFilms", function() { return searchFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmById", function() { return getFilmById; });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./types */ "./store/films/types.js");
/* harmony import */ var _apis_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../apis/services */ "./apis/services.js");
/* harmony import */ var _apiCall__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../apiCall */ "./store/apiCall.js");



const fetchFilms = url => {
  const URI = url ? Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["filmsFetch"])(url) : Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["filmsFetch"])();
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_FILMS"], () => URI, data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const searchFilms = name => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_FILMS"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["searchFilmsByName"])(name), data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const getFilmById = id => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["GET_FILM_INFO"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["getFilm"])(id), response => {
      const title = response.data.title;
      const director = response.data.director;
      const producer = response.data.producer;
      const releaseDate = response.data.release_date;
      const vehicles = response.data.vehicles;
      const characters = response.data.characters;
      const planets = response.data.planets;
      return {
        title,
        director,
        producer,
        releaseDate,
        vehicles,
        characters,
        planets
      };
    }));
  };
};

/***/ }),

/***/ "./store/films/selectors.js":
/*!**********************************!*\
  !*** ./store/films/selectors.js ***!
  \**********************************/
/*! exports provided: getAllFilms, getFilmById, isLoadingFilms, getTotalFilms, getFilmsNextPage, getFilmsPreviousPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllFilms", function() { return getAllFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmById", function() { return getFilmById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingFilms", function() { return isLoadingFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalFilms", function() { return getTotalFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmsNextPage", function() { return getFilmsNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmsPreviousPage", function() { return getFilmsPreviousPage; });
const getAllFilms = films => films.items;
const getFilmById = films => films.itemSelected;
const isLoadingFilms = films => films.isLoading;
const getTotalFilms = films => films.totalItems;
const getFilmsNextPage = films => films.next;
const getFilmsPreviousPage = films => films.previous;

/***/ }),

/***/ "./store/films/types.js":
/*!******************************!*\
  !*** ./store/films/types.js ***!
  \******************************/
/*! exports provided: FETCH_FILMS, FETCH_FILMS_START, FETCH_FILMS_SUCCESS, FETCH_FILMS_FAIL, GET_FILM_INFO, GET_FILM_INFO_START, GET_FILM_INFO_SUCCESS, GET_FILM_INFO_FAIL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_FILMS", function() { return FETCH_FILMS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_FILMS_START", function() { return FETCH_FILMS_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_FILMS_SUCCESS", function() { return FETCH_FILMS_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_FILMS_FAIL", function() { return FETCH_FILMS_FAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_FILM_INFO", function() { return GET_FILM_INFO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_FILM_INFO_START", function() { return GET_FILM_INFO_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_FILM_INFO_SUCCESS", function() { return GET_FILM_INFO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_FILM_INFO_FAIL", function() { return GET_FILM_INFO_FAIL; });
const FETCH_FILMS = 'FETCH_FILMS';
const FETCH_FILMS_START = 'FETCH_FILMS_START';
const FETCH_FILMS_SUCCESS = 'FETCH_FILMS_SUCCESS';
const FETCH_FILMS_FAIL = 'FETCH_FILMS_FAIL';
const GET_FILM_INFO = 'GET_FILM_INFO';
const GET_FILM_INFO_START = 'GET_FILM_INFO_START';
const GET_FILM_INFO_SUCCESS = 'GET_FILM_INFO_SUCCESS';
const GET_FILM_INFO_FAIL = 'GET_FILM_INFO_FAIL';

/***/ }),

/***/ "./store/operations.js":
/*!*****************************!*\
  !*** ./store/operations.js ***!
  \*****************************/
/*! exports provided: fetchCharacters, searchCharacters, getCharacterById, fetchFilms, searchFilms, getFilmById, fetchPlanets, searchPlanets, getPlanetById, fetchVehicles, searchVehicles, getVehicleById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _characters_operations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./characters/operations */ "./store/characters/operations.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fetchCharacters", function() { return _characters_operations__WEBPACK_IMPORTED_MODULE_0__["fetchCharacters"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "searchCharacters", function() { return _characters_operations__WEBPACK_IMPORTED_MODULE_0__["searchCharacters"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getCharacterById", function() { return _characters_operations__WEBPACK_IMPORTED_MODULE_0__["getCharacterById"]; });

/* harmony import */ var _films_operations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./films/operations */ "./store/films/operations.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fetchFilms", function() { return _films_operations__WEBPACK_IMPORTED_MODULE_1__["fetchFilms"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "searchFilms", function() { return _films_operations__WEBPACK_IMPORTED_MODULE_1__["searchFilms"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getFilmById", function() { return _films_operations__WEBPACK_IMPORTED_MODULE_1__["getFilmById"]; });

/* harmony import */ var _planets_operations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./planets/operations */ "./store/planets/operations.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fetchPlanets", function() { return _planets_operations__WEBPACK_IMPORTED_MODULE_2__["fetchPlanets"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "searchPlanets", function() { return _planets_operations__WEBPACK_IMPORTED_MODULE_2__["searchPlanets"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getPlanetById", function() { return _planets_operations__WEBPACK_IMPORTED_MODULE_2__["getPlanetById"]; });

/* harmony import */ var _vehicles_operations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vehicles/operations */ "./store/vehicles/operations.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fetchVehicles", function() { return _vehicles_operations__WEBPACK_IMPORTED_MODULE_3__["fetchVehicles"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "searchVehicles", function() { return _vehicles_operations__WEBPACK_IMPORTED_MODULE_3__["searchVehicles"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getVehicleById", function() { return _vehicles_operations__WEBPACK_IMPORTED_MODULE_3__["getVehicleById"]; });






/***/ }),

/***/ "./store/planets/operations.js":
/*!*************************************!*\
  !*** ./store/planets/operations.js ***!
  \*************************************/
/*! exports provided: fetchPlanets, searchPlanets, getPlanetById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchPlanets", function() { return fetchPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchPlanets", function() { return searchPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetById", function() { return getPlanetById; });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./types */ "./store/planets/types.js");
/* harmony import */ var _apis_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../apis/services */ "./apis/services.js");
/* harmony import */ var _apiCall__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../apiCall */ "./store/apiCall.js");



const fetchPlanets = url => {
  const URI = url ? Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["planetsFetch"])(url) : Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["planetsFetch"])();
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_PLANETS"], () => URI, data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const searchPlanets = name => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_PLANETS"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["searchPlanetsByName"])(name), data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const getPlanetById = id => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["GET_PLANET_INFO"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["getPlanet"])(id), response => {
      const name = response.data.name;
      const diameter = response.data.diameter;
      const rotation = response.data.rotation_period;
      const orbital = response.data.orbital_period;
      const gravity = response.data.gravity;
      const population = response.data.population;
      const climate = response.data.climate;
      const terrain = response.data.terrain;
      const surfaceWater = response.data.surface_water;
      const residents = response.data.residents;
      const films = response.data.films;
      return {
        name,
        diameter,
        rotation,
        orbital,
        gravity,
        population,
        climate,
        terrain,
        surfaceWater,
        residents,
        films
      };
    }));
  };
};

/***/ }),

/***/ "./store/planets/selectors.js":
/*!************************************!*\
  !*** ./store/planets/selectors.js ***!
  \************************************/
/*! exports provided: getAllPlanets, getPlanetById, isLoadingPlanets, getTotalPlanets, getPlanetsNextPage, getPlanetsPreviousPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllPlanets", function() { return getAllPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetById", function() { return getPlanetById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingPlanets", function() { return isLoadingPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalPlanets", function() { return getTotalPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetsNextPage", function() { return getPlanetsNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetsPreviousPage", function() { return getPlanetsPreviousPage; });
const getAllPlanets = planets => planets.items;
const getPlanetById = planets => planets.itemSelected;
const isLoadingPlanets = planets => planets.isLoading;
const getTotalPlanets = planets => planets.totalItems;
const getPlanetsNextPage = planets => planets.next;
const getPlanetsPreviousPage = planets => planets.previous;

/***/ }),

/***/ "./store/planets/types.js":
/*!********************************!*\
  !*** ./store/planets/types.js ***!
  \********************************/
/*! exports provided: FETCH_PLANETS, FETCH_PLANETS_START, FETCH_PLANETS_SUCCESS, FETCH_PLANETS_FAIL, GET_PLANET_INFO, GET_PLANET_INFO_START, GET_PLANET_INFO_SUCCESS, GET_PLANET_INFO_FAIL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PLANETS", function() { return FETCH_PLANETS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PLANETS_START", function() { return FETCH_PLANETS_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PLANETS_SUCCESS", function() { return FETCH_PLANETS_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PLANETS_FAIL", function() { return FETCH_PLANETS_FAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_PLANET_INFO", function() { return GET_PLANET_INFO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_PLANET_INFO_START", function() { return GET_PLANET_INFO_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_PLANET_INFO_SUCCESS", function() { return GET_PLANET_INFO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_PLANET_INFO_FAIL", function() { return GET_PLANET_INFO_FAIL; });
const FETCH_PLANETS = 'FETCH_PLANETS';
const FETCH_PLANETS_START = 'FETCH_PLANETS_START';
const FETCH_PLANETS_SUCCESS = 'FETCH_PLANETS_SUCCESS';
const FETCH_PLANETS_FAIL = 'FETCH_PLANETS_FAIL';
const GET_PLANET_INFO = 'GET_PLANET_INFO';
const GET_PLANET_INFO_START = 'GET_PLANET_INFO_START';
const GET_PLANET_INFO_SUCCESS = 'GET_PLANET_INFO_SUCCESS';
const GET_PLANET_INFO_FAIL = 'GET_PLANET_INFO_FAIL';

/***/ }),

/***/ "./store/selectors.js":
/*!****************************!*\
  !*** ./store/selectors.js ***!
  \****************************/
/*! exports provided: getAllCharacters, getCharacterById, isLoadingCharacters, getTotalCharacters, getCharactersNextPage, getCharactersPreviousPage, getAllPlanets, getPlanetById, isLoadingPlanets, getTotalPlanets, getPlanetsNextPage, getPlanetsPreviousPage, getAllVehicles, getVehicleById, isLoadingVehicles, getTotalVehicles, getVehiclesNextPage, getVehiclesPreviousPage, getAllFilms, getFilmById, isLoadingFilms, getTotalFilms, getFilmsNextPage, getFilmsPreviousPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllCharacters", function() { return getAllCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharacterById", function() { return getCharacterById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingCharacters", function() { return isLoadingCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalCharacters", function() { return getTotalCharacters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharactersNextPage", function() { return getCharactersNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCharactersPreviousPage", function() { return getCharactersPreviousPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllPlanets", function() { return getAllPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetById", function() { return getPlanetById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingPlanets", function() { return isLoadingPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalPlanets", function() { return getTotalPlanets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetsNextPage", function() { return getPlanetsNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanetsPreviousPage", function() { return getPlanetsPreviousPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllVehicles", function() { return getAllVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehicleById", function() { return getVehicleById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingVehicles", function() { return isLoadingVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalVehicles", function() { return getTotalVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehiclesNextPage", function() { return getVehiclesNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehiclesPreviousPage", function() { return getVehiclesPreviousPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllFilms", function() { return getAllFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmById", function() { return getFilmById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingFilms", function() { return isLoadingFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalFilms", function() { return getTotalFilms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmsNextPage", function() { return getFilmsNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilmsPreviousPage", function() { return getFilmsPreviousPage; });
/* harmony import */ var _characters_selectors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./characters/selectors */ "./store/characters/selectors.js");
/* harmony import */ var _planets_selectors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./planets/selectors */ "./store/planets/selectors.js");
/* harmony import */ var _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vehicles/selectors */ "./store/vehicles/selectors.js");
/* harmony import */ var _films_selectors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./films/selectors */ "./store/films/selectors.js");




const getAllCharacters = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["getAllCharacters"](state.characters);
const getCharacterById = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["getCharacterById"](state.characters);
const isLoadingCharacters = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["isLoadingCharacters"](state.characters);
const getTotalCharacters = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["getTotalCharacters"](state.characters);
const getCharactersNextPage = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["getCharactersNextPage"](state.characters);
const getCharactersPreviousPage = state => _characters_selectors__WEBPACK_IMPORTED_MODULE_0__["getCharactersPreviousPage"](state.characters);
const getAllPlanets = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["getAllPlanets"](state.planets);
const getPlanetById = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["getPlanetById"](state.planets);
const isLoadingPlanets = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["isLoadingPlanets"](state.planets);
const getTotalPlanets = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["getTotalPlanets"](state.planets);
const getPlanetsNextPage = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["getPlanetsNextPage"](state.planets);
const getPlanetsPreviousPage = state => _planets_selectors__WEBPACK_IMPORTED_MODULE_1__["getPlanetsPreviousPage"](state.planets);
const getAllVehicles = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["getAllVehicles"](state.vehicles);
const getVehicleById = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["getVehicleById"](state.vehicles);
const isLoadingVehicles = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["isLoadingVehicles"](state.vehicles);
const getTotalVehicles = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["getTotalVehicles"](state.vehicles);
const getVehiclesNextPage = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["getVehiclesNextPage"](state.vehicles);
const getVehiclesPreviousPage = state => _vehicles_selectors__WEBPACK_IMPORTED_MODULE_2__["getVehiclesPreviousPage"](state.vehicles);
const getAllFilms = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["getAllFilms"](state.films);
const getFilmById = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["getFilmById"](state.films);
const isLoadingFilms = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["isLoadingFilms"](state.films);
const getTotalFilms = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["getTotalFilms"](state.films);
const getFilmsNextPage = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["getFilmsNextPage"](state.films);
const getFilmsPreviousPage = state => _films_selectors__WEBPACK_IMPORTED_MODULE_3__["getFilmsPreviousPage"](state.films);

/***/ }),

/***/ "./store/vehicles/operations.js":
/*!**************************************!*\
  !*** ./store/vehicles/operations.js ***!
  \**************************************/
/*! exports provided: fetchVehicles, searchVehicles, getVehicleById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchVehicles", function() { return fetchVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchVehicles", function() { return searchVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehicleById", function() { return getVehicleById; });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./types */ "./store/vehicles/types.js");
/* harmony import */ var _apis_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../apis/services */ "./apis/services.js");
/* harmony import */ var _apiCall__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../apiCall */ "./store/apiCall.js");



const fetchVehicles = url => {
  const URI = url ? Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["vehiclesFetch"])(url) : Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["vehiclesFetch"])();
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_VEHICLES"], () => URI, data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const searchVehicles = name => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["FETCH_VEHICLES"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["searchVehiclesByName"])(name), data => {
      const next = data.next;
      const previous = data.previous;
      const total = data.count;
      const response = data.results;
      return {
        response,
        total,
        next,
        previous
      };
    }));
  };
};
const getVehicleById = id => {
  return async dispatch => {
    return dispatch(Object(_apiCall__WEBPACK_IMPORTED_MODULE_2__["default"])(_types__WEBPACK_IMPORTED_MODULE_0__["GET_VEHICLE_INFO"], () => Object(_apis_services__WEBPACK_IMPORTED_MODULE_1__["getVehicle"])(id), response => {
      const model = response.data.id;
      const name = response.data.name;
      const vehicleClass = response.data.vehicle_class;
      const credits = response.data.cost_in_credits;
      const passengers = response.data.passengers;
      const speed = response.data.max_atmosphering_speed;
      const capacity = response.data.cargo_capacity;
      const films = response.data.films;
      const pilots = response.data.pilots;
      return {
        name,
        model,
        vehicleClass,
        credits,
        passengers,
        speed,
        capacity,
        films,
        pilots
      };
    }));
  };
};

/***/ }),

/***/ "./store/vehicles/selectors.js":
/*!*************************************!*\
  !*** ./store/vehicles/selectors.js ***!
  \*************************************/
/*! exports provided: getAllVehicles, getVehicleById, isLoadingVehicles, getTotalVehicles, getVehiclesNextPage, getVehiclesPreviousPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllVehicles", function() { return getAllVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehicleById", function() { return getVehicleById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoadingVehicles", function() { return isLoadingVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTotalVehicles", function() { return getTotalVehicles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehiclesNextPage", function() { return getVehiclesNextPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVehiclesPreviousPage", function() { return getVehiclesPreviousPage; });
const getAllVehicles = vehicles => vehicles.items;
const getVehicleById = vehicles => vehicles.itemSelected;
const isLoadingVehicles = vehicles => vehicles.isLoading;
const getTotalVehicles = vehicles => vehicles.totalItems;
const getVehiclesNextPage = vehicles => vehicles.next;
const getVehiclesPreviousPage = vehicles => vehicles.previous;

/***/ }),

/***/ "./store/vehicles/types.js":
/*!*********************************!*\
  !*** ./store/vehicles/types.js ***!
  \*********************************/
/*! exports provided: FETCH_VEHICLES, FETCH_VEHICLES_START, FETCH_VEHICLES_SUCCESS, FETCH_VEHICLES_FAIL, GET_VEHICLE_INFO, GET_VEHICLE_INFO_START, GET_VEHICLE_INFO_SUCCESS, GET_VEHICLE_INFO_FAIL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_VEHICLES", function() { return FETCH_VEHICLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_VEHICLES_START", function() { return FETCH_VEHICLES_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_VEHICLES_SUCCESS", function() { return FETCH_VEHICLES_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_VEHICLES_FAIL", function() { return FETCH_VEHICLES_FAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_VEHICLE_INFO", function() { return GET_VEHICLE_INFO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_VEHICLE_INFO_START", function() { return GET_VEHICLE_INFO_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_VEHICLE_INFO_SUCCESS", function() { return GET_VEHICLE_INFO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_VEHICLE_INFO_FAIL", function() { return GET_VEHICLE_INFO_FAIL; });
const FETCH_VEHICLES = 'FETCH_VEHICLES';
const FETCH_VEHICLES_START = 'FETCH_VEHICLES_START';
const FETCH_VEHICLES_SUCCESS = 'FETCH_VEHICLES_SUCCESS';
const FETCH_VEHICLES_FAIL = 'FETCH_VEHICLES_FAIL';
const GET_VEHICLE_INFO = 'GET_VEHICLE_INFO';
const GET_VEHICLE_INFO_START = 'GET_VEHICLE_INFO_START';
const GET_VEHICLE_INFO_SUCCESS = 'GET_VEHICLE_INFO_SUCCESS';
const GET_VEHICLE_INFO_FAIL = 'GET_VEHICLE_INFO_FAIL';

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/lucasgiuri/Desktop/beezy/app/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "core-js/library/fn/map":
/*!*****************************************!*\
  !*** external "core-js/library/fn/map" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/map");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/create":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/create" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/create");

/***/ }),

/***/ "core-js/library/fn/object/define-properties":
/*!**************************************************************!*\
  !*** external "core-js/library/fn/object/define-properties" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptors":
/*!*************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptors" ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-symbols":
/*!*********************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-symbols" ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "core-js/library/fn/object/keys":
/*!*************************************************!*\
  !*** external "core-js/library/fn/object/keys" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "core-js/library/fn/promise":
/*!*********************************************!*\
  !*** external "core-js/library/fn/promise" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/promise");

/***/ }),

/***/ "core-js/library/fn/weak-map":
/*!**********************************************!*\
  !*** external "core-js/library/fn/weak-map" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/weak-map");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-is");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map