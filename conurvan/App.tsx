import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Button from '@material-ui/core/Button';


export default function App() {
  return (
    <View style={styles.container}>
      <Text>BARNADRID!</Text>
      <Button variant="contained" color="primary">
        Hola Mundo!
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
